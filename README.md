# Visual Studio Code (Code-Server) Container for Docker with php 8.1 Support

## <u>First Start</u>

Clone the Repository (http)

    git clone https://gitlab.com/MarJun1988/mj-docker-vscode.git

Clone the Repository (ssh)

    git@gitlab.com:MarJun1988/mj-docker-vscode.git

Open the Folder

    cd mj-docker-vscode

Run the docker-compose File with

    docker-compose -p "vsCode" up -d

Explanation of the parameters

    -p "NAME" => Projectname of the Container (Stackname)

    -d => detached mode

## <u>Webinterface for the Code-Server Instance</u>

Address in the browser without SSL

[http://127.0.0.1:1111](http://127.0.0.1:1111)

### <u>Default Password</u>

    epfAQDQQz0JiqW8Auk8x

## <u>Original source without PHP</u>

[https://hub.docker.com/r/linuxserver/code-server](https://hub.docker.com/r/linuxserver/code-server)
